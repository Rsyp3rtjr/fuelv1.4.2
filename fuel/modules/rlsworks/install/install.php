<?php 
$config['name'] = 'Rlsworks Module';
$config['version'] = RLSWORKS_VERSION;
$config['author'] = '';
$config['company'] = '';
$config['license'] = 'Apache 2';
$config['copyright'] = '';
$config['author_url'] = '';
$config['description'] = '';
$config['compatibility'] = '1.0';
$config['instructions'] = '';
$config['permissions'] = 'rlsworks';
$config['migration_version'] = 0;
$config['install_sql'] = '';
$config['uninstall_sql'] = '';
$config['repo'] = '';